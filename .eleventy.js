const htmlmin = require('html-minifier');
const dateFns = require('date-fns');
const lazyImagesPlugin = require('eleventy-plugin-lazyimages');
const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight');
const i18n = require('eleventy-plugin-i18n');
const translations = require('./src/_data/i18n');

module.exports = function (eleventyConfig) {
  // PLUGINS **************************************************************

  eleventyConfig.addPlugin(syntaxHighlight);

  // does not work with background images
  eleventyConfig.addPlugin(lazyImagesPlugin, {
    transformImgPath: (imgPath) => {
      if (imgPath.startsWith('http://') || imgPath.startsWith('https://')) {
        // Handle remote file
        return imgPath;
      } else {
        return `./src/${imgPath}`;
      }
    },
  });

  eleventyConfig.addPlugin(i18n, {
    translations,
    fallbackLocales: {
      '*': 'en-US',
    },
  });

  eleventyConfig.setEjsOptions({
    rmWhitespace: true,
    context: {
      dateFns,
    },
  });

  eleventyConfig.setBrowserSyncConfig({
    files: './_site/assets/styles/main.css',
  });

  // use this to redirect to en-US
  // eleventyConfig.setBrowserSyncConfig({
  //   files: './_site/assets/styles/main.css',
  //   callbacks: {
  //     ready: function (err, bs) {
  //       bs.addMiddleware('*', (req, res) => {
  //         if (req.url === '/') {
  //           res.writeHead(302, {
  //             location: '/en-US/'
  //           });
  //           res.end();
  //         }
  //       });
  //     }
  //   }
  // });

  eleventyConfig.addPassthroughCopy('styles/fonts');
  eleventyConfig.addPassthroughCopy('src/assets/images/**/*.png');
  eleventyConfig.addPassthroughCopy('src/assets/images/**/*.svg');
  eleventyConfig.addPassthroughCopy('src/assets/images/**/*.jpg');
  eleventyConfig.addPassthroughCopy('node_modules/@glidejs/glide/dist');
  eleventyConfig.addPassthroughCopy('node_modules/blueimp-md5/js');

  eleventyConfig.addTransform('htmlmin', (content, outputPath) => {
    if (outputPath.endsWith('.html')) {
      const minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true,
        minifyJS: true,
      });
      return minified;
    }

    return content;
  });

  // Import fast-glob package
  const fg = require('fast-glob');

  // Run search for images in /screenshots
  const galleryThumbnails = fg.sync(['**/images/screenshots/*resized*', '!**/_site']);
  const galleryImagesAll = fg.sync(['**/images/screenshots/*', '!**/_site']);

  //Create collection of screenshots
  eleventyConfig.addCollection('screenshotThumbnails', function (collection) {
    return galleryThumbnails.map((url) => {
      return url.replace('src/', '');
    });
  });

  //Create collection of screenshots
  eleventyConfig.addCollection('screenshotHires', function (collection) {
    return galleryImagesAll
      .filter((url) => !url.includes('resized'))
      .map((url) => {
        return url.replace('src/', '');
      });
  });

  return {
    passthroughFileCopy: true,
    dir: { input: 'src', output: '_site', data: '_data' },
  };

  // return {
  //   templateFormats: ['md', 'njk', 'html', 'liquid'],

  //   // If your site lives in a different subdirectory, change this.
  //   // Leading or trailing slashes are all normalized away, so don’t worry about it.
  //   // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
  //   // This is only used for URLs (it does not affect your file structure)
  //   pathPrefix: '/',

  //   markdownTemplateEngine: 'liquid',
  //   htmlTemplateEngine: 'njk',
  //   dataTemplateEngine: 'njk',
  //   passthroughFileCopy: true,
  //   dir: {
  //     input: '.',
  //     includes: '_includes',
  //     data: '_data',
  //     output: 'docs',
  //   },
  // };
};
