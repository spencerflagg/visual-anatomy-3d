module.exports = [
  {
    name: 'Andre Taris',
    quote: 'I am a designer and researcher and as part of TU Delft’s medical design major you get to do a surgical level anatomy class at Erasmus hospital. A world first and leader in this type of collab this involved full dissection training of us medical designers and common procedure run throughs. Laparoscopic training and this is more thorough than most doctors gets hands on. This app aids at that level',
    stars: 5,
    site: 'App Store',
    link: ''
  },
  {
    name: 'Anthony Horsley',
    quote: 'I\'m homeschooled and this is awesome!',
    stars: 5,
    site: 'App Store',
    link: ''
  },
  {
    name: 'Tom van Vugt',
    quote: 'As a physical therapist this is one of the better apps out there for learning human anatomy, I tried multiple, but this one is easy to use and runs smoothly with lots of options.',
    stars: 5,
    site: 'App Store',
    link: ''
  }
]