module.exports = [
  { slug: 'skeletal', name: 'Skeletal', status: 'free', order: 1 },
  { slug: 'ligamentous', name: 'Ligamentous', status: 'free', order: 2 },
  { slug: 'muscular', name: 'Muscular', status: 'pro', order: 3 },
  { slug: 'digestive', name: 'Digestive', status: 'pro', order: 4 },
  { slug: 'respiratory', name: 'Respiratory', status: 'pro', order: 5 },
  { slug: 'urogenital', name: 'Urogenital', status: 'pro', order: 6 },
  { slug: 'endocrine', name: 'Endocrine', status: 'pro', order: 7 },
  { slug: 'circulatory', name: 'Circulatory', status: 'soon', order: 8 },
  { slug: 'nervous', name: 'Nervous', status: 'soon', order: 9 },
  { slug: 'lymphatic', name: 'Lymphatic', status: 'soon', order: 10 },
  { slug: 'integumentary', name: 'Integumentary', status: 'soon', order: 11 }
]